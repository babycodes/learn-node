class Person {
	constructor(name='unknown', age=0) {
		this.name = name
		this.age = age
	}

	saySomething() {
		return `hello my name is ${this.name} and i'm ${this.age}`
	}
}

module.exports = Person