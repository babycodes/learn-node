const path = require('path');

// Basename get Base file name
console.log(path.basename(__filename))

// directory name
console.log(path.dirname(__filename))

// File Extention
console.log(path.extname(__filename))

// create path object
console.log(path.parse(__filename))