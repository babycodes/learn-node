const url = require('url')

const myUrl = new URL('http://myweb.com:8000/hello/?id=1')

console.log(myUrl.href)
console.log(myUrl.host)
console.log(myUrl.hostname)
console.log(myUrl.pathname)
console.log(myUrl.search)
console.log(myUrl.searchParams)
myUrl.searchParams.append('name','wahyu')
console.log(myUrl.searchParams)
myUrl.searchParams.forEach((v, n) => {
	console.log(n, ' : ', v)
})